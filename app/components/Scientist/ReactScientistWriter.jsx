var React = require('react')
var ReactDOM = require('pubsweet-core/node_modules/react-dom')

import { bindActionCreators } from 'pubsweet-core/node_modules/redux'
import { connect } from 'pubsweet-core/node_modules/react-redux'
import * as Actions from 'pubsweet-core/app/actions'
var Editor = require('substance-scientist/examples/jats-editor/JATSEditor.js')
var Configurator = require('substance-scientist/packages/common/BaseConfigurator')
var Component = require('substance-scientist/node_modules/substance/ui/Component')
var DocumentSession = require('substance-scientist/node_modules/substance/model/DocumentSession')
var request = require('substance-scientist/node_modules/substance/util/request')

var PublisherPackage = require('substance-scientist/packages/publisher/package')
var PubsweetXMLStore = require('./PubsweetXMLStore')
var path = require('path')

// Jats-editor wrapped in a React component
// ------------------

class ReactScientistWriter extends React.Component {

  getWriter () {
    return this
  }

  createDocumentSession () {
    return new DocumentSession('')
  }

  // New props arrived, update the editor
  componentDidUpdate () {
    var documentSession = this.createDocumentSession()

    this.writer.extendProps({
      documentSession: documentSession
    })
  }

  save (source, changes, callback) {
    // var exporter = new LensArticleExporter()
    // this.props.onSave(exporter.exportDocument(source), callback)
    console.log('do nothing for now')
  }

  componentDidMount () {
    var self = this
    PubsweetXMLStore.prototype.readXML = function(documentId, cb) {
      if (self.props.fragment && self.props.fragment.source) {
        return cb(null, self.props.fragment.source)
      }
      return cb(null,
          '<?xml version="1.0" encoding="UTF-8"?>' +
          '<!DOCTYPE article PUBLIC "-//NLM//DTD JATS (Z39.96) Journal Archiving and Interchange DTD v1.1d3 20150301//EN"  "JATS-archivearticle1.dtd">' +
          '<article article-type="sample" dtd-version="1.1d3" xmlns:xlink="http://www.w3.org/1999/xlink">' +
            '<front>' +
              '<article-meta></article-meta>' +
            '</front>' +
            '<body><p id="p1">Empty Sample</p></body>' +
            '<back></back>' +
          '</article>')

      }
    PubsweetXMLStore.prototype.writeXML = function (documentId, xml, cb) {
      self.props.onSave(xml, cb)
    }

    var el = ReactDOM.findDOMNode(this)

    var configurator = new Configurator({
      name: 'pubsweet-jats-editor',
      configure: function (config) {
        // Base package with regular JATS support
        config.import(PublisherPackage)

        // Define XML Store
        config.setXMLStore(PubsweetXMLStore)
        config.addStyle(path.join(__dirname, 'ScientistWriter.scss'))
      }
    })

    this.writer = Component.mount(Editor, {
      documentId: 'vanilla',
      configurator: configurator
    }, el)
  }

  componentWillUnmount () {
    this.writer.dispose()
  }

  render () {

    let editor

    if (this.props.fragment) {
      editor = React.DOM.div({
        className: 'scientist-writer-wrapper'
      })
    } else {
      editor = <p>Loading</p>
    }

    return (
        <div>
          {editor}
        </div>
    )
  }
}

ReactScientistWriter.propTypes = {
  fragment: React.PropTypes.object,
  id: React.PropTypes.string.isRequired,
  onSave: React.PropTypes.func
}

function mapStateToProps (state, ownProps) {
  return {
    id: ownProps.id,
    fragment: _.find(state.fragments, function (f) {
      return f.id === ownProps.id
    })
  }
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ReactScientistWriter)
