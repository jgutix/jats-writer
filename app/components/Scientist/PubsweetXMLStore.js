'use strict'

var oo = require('substance-scientist/node_modules/substance/util/oo')
var request = require('substance-scientist/node_modules/substance/util/request')

function PubsweetXMLStore () {

}

PubsweetXMLStore.Prototype = function () {
  this.readXML = function (documentId, cb) {
    var cached = localStorage.getItem(documentId)
    if (cached) {
      return cb(null, cached)
    }
    request('GET', '/data/' + documentId + '.xml', null, cb)
  }

  // TODO make functional
  this.writeXML = function (documentId, xml, cb) {
    localStorage.setItem(documentId, xml)
    cb(null)
  }
}

oo.initClass(PubsweetXMLStore)

module.exports = PubsweetXMLStore
