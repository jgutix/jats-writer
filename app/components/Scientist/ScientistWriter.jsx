import React from 'react'
import ReactScientistWriter from './ReactScientistWriter'
import { bindActionCreators } from 'pubsweet-core/node_modules/redux'
import { connect } from 'pubsweet-core/node_modules/react-redux'
import * as Actions from 'pubsweet-core/app/actions'

// Styles
import './ScientistWriter.scss'

class ScientistWriter extends React.Component {
  constructor (props) {
    super(props)
    this.save = this.save.bind(this)
    this.uploadFile = this.uploadFile.bind(this)
  }

  save (source, callback) {
    let doc = Object.assign(this.props.fragment, {
      source: source
    })
    this.props.actions.updateFragment(doc)

    callback(null, source)
  }

  uploadFile (file, callback) {
    return this.props.uploadFile(file, callback)
  }

  render () {
    return <ReactScientistWriter
      documentId={this.props.fragment.id}
      id={this.props.fragment.id}
      version={this.props.fragment.version}
      content={this.props.fragment.source}
      onSave={this.save}
      />
  }
}

ScientistWriter.propTypes = {
  fragment: React.PropTypes.object,
  actions: React.PropTypes.object,
  save: React.PropTypes.func,
  uploadFile: React.PropTypes.func,
  id: React.PropTypes.string.isRequired
}

// export default ScientistWriter
function mapStateToProps (state, ownProps) {
  return {
    id: ownProps.params.id,
    fragment: _.find(state.fragments, function (f) {
      return f.id === ownProps.params.id
    })
  }
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ScientistWriter)
